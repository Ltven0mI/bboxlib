local systems = {}

local _systemsTable = {}
local _enabledSystemsTable = {}


local _settings = nil
local _defaultSettings = {
    systemFileDirectory="systems" -- The directory system files will be loaded from.
}


local function _loadSystemFile(filePath)
    local fileInfo = love.filesystem.getInfo(filePath, "file")
    if fileInfo ~= nil then
        local chunk, err = love.filesystem.load(filePath)
        local system = chunk()
        assert(systems.isValid(system), "Attempted to load invalid system: '"..filePath.."'")
        print("[LOG](BBoxLib) | Successfully loaded System: '"..system._header.name.."'")
    end
end

local function _loadSystemFiles(directory)
    local directoryInfo = love.filesystem.getInfo(directory, "directory")
    if directoryInfo == nil then
        print("[WARN](BBoxLib) No systems directory exists at '"..directory.."'")
        return
    end
    
    print("[LOG](BBoxLib) Loading Systems...")
    local directoryItems = love.filesystem.getDirectoryItems(directory)
    for _, item in ipairs(directoryItems) do
        _loadSystemFile(directory.."/"..item)
    end
    print("[LOG](BBoxLib) Finished Loading Systems!")
end


function systems.init(settings)
    _settings = settings or {}
    setmetatable(_settings, { __index=_defaultSettings })
    _loadSystemFiles(_settings.systemFileDirectory)
end


function _singleSystemCallback(system, callbackName, ...)
    if system[callbackName] then
        system[callbackName](...)
    end
end

-- ? Is this function name descriptive enough?
function systems.callback(callbackName, ...)
    for systemName, system in pairs(_enabledSystemsTable) do
        _singleSystemCallback(system, callbackName, ...)
    end
end

function systems.enable(systemName)
    assert(type(systemName) == "string", "systems.enable expects a string!")
    assert(_systemsTable[systemName], "Attempted to enable unknown system '"..systemName.."'")

    if _enabledSystemsTable[systemName] ~= nil then
        print("[WARN](BBoxLib) Attempted to enable already enabled system '"..systemName.."'")
        return
    end
    _enabledSystemsTable[systemName] = _systemsTable[systemName]
    _singleSystemCallback(_systemsTable[systemName], "enabled")
end

function systems.disable(systemName)
    assert(type(systemName) == "string", "systems.disable expects a string!")
    assert(_systemsTable[systemName], "Attempted to disable unknown system '"..systemName.."'")

    if _enabledSystemsTable[systemName] == nil then
        print("[WARN](BBoxLib) Attempted to disable already disabled system '"..systemName.."'")
        return
    end
    _enabledSystemsTable[systemName] = nil
    _singleSystemCallback(_systemsTable[systemName], "disabled")
end


function systems.isValid(system)
    if type(system) ~= "table" then return false end
    if type(system._header) ~= "table" then return false end
    if type(system._header.name) ~= "string" then return false end
    return true
end

function systems.new(header)
    assert(type(header)=="table", "'new' expected type 'table' not '"..type(header).."'")
    assert(type(header.name)=="string", "argument #1 must contain a 'name' key with type string!")
    
    assert(_systemsTable[header.name]==nil, "System already exists with name '"..header.name.."'")
    
    local system = {
        _header=header
    }
    
    _systemsTable[header.name] = system
    
    return system
end

return systems