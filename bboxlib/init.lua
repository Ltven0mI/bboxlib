local module_path = ...
local bbox = {}

local function loadModule(path)
  local chunk, err = love.filesystem.load(path)
  if chunk == nil then
    error("Failed to load module: '"..path.."' : "..err)
  end
  return chunk()
end

bbox.states = loadModule(module_path.."/states.lua")
bbox.systems = loadModule(module_path.."/systems.lua")

function bbox.init()
  bbox.states.init()
  bbox.systems.init()
end

return bbox