local states = {}

local _activeState = nil
local _statesTable = {}

local _settings = nil
local _defaultSettings = {
    stateFileDirectory="states" -- The directory state files will be loaded from.
}


local function _loadStateFile(filePath)
    local fileInfo = love.filesystem.getInfo(filePath, "file")
    if fileInfo ~= nil then
        local chunk, err = love.filesystem.load(filePath)
        local state = chunk()
        assert(states.isValid(state), "Attempted to load invalid state: '"..filePath.."'")
        print("[LOG](BBoxLib) | Successfully loaded State: '"..state._header.name.."'")
    end
end

local function _loadStateFiles(directory)
    local directoryInfo = love.filesystem.getInfo(directory, "directory")
    if directoryInfo == nil then
        print("[WARN](BBoxLib) No states directory exists at '"..directory.."'")
        return
    end
    
    print("[LOG](BBoxLib) Loading States...")
    local directoryItems = love.filesystem.getDirectoryItems(directory)
    for _, item in ipairs(directoryItems) do
        _loadStateFile(directory.."/"..item)
    end
    print("[LOG](BBoxLib) Finished Loading States!")
end


function states.init(settings)
    _settings = settings or {}
    setmetatable(_settings, { __index=_defaultSettings })
    _loadStateFiles(_settings.stateFileDirectory)
end

-- ? Is this function name descriptive enough?
function states.callback(callbackName, ...)
    if _activeState == nil then
        return
    end
    if _activeState[callbackName] then
        _activeState[callbackName](...)
    end
end

function states.switchTo(stateName)
    states.callback("switchedFrom")
    
    if stateName == nil then
        if _activeState == nil then
            print("[WARN](BBoxLib) Attempted to switch from empty state to another empty state!")
        end
        _activeState = nil
        return
    end
    
    assert(type(stateName)=="string", "'switchTo' expects type 'string' not '"..type(stateName).."'")
    assert(_statesTable[stateName], "Unknown state '"..stateName.."'")
    
    _activeState = _statesTable[stateName]
    
    states.callback("switchedTo")
end

function states.isValid(state)
    if type(state) ~= "table" then return false end
    if type(state._header) ~= "table" then return false end
    if type(state._header.name) ~= "string" then return false end
    return true
end

function states.new(header)
    assert(type(header)=="table", "'new' expected type 'table' not '"..type(header).."'")
    assert(type(header.name)=="string", "argument #1 must contain a 'name' key with type string!")
    
    assert(_statesTable[header.name]==nil, "State already exists with name '"..header.name.."'")
    
    local state = {
        _header=header
    }
    
    _statesTable[header.name] = state
    
    return state
end

return states