local bboxlib = require("bboxlib")

function love.load()
    bboxlib.init()
    bboxlib.states.switchTo("example")
    bboxlib.systems.enable("example")
end

function love.draw()
    bboxlib.states.callback("draw")
    bboxlib.systems.callback("draw")
end

function love.keypressed(key)
    if key == "space" then
        bboxlib.states.switchTo(nil)
    end
    if key == "return" then
        bboxlib.systems.disable("example")
    end
end