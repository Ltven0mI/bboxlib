local bboxlib = require("bboxlib")
local example = bboxlib.systems.new({name="example"})

function example.draw()
    local sw, sh = love.graphics.getDimensions()
    love.graphics.rectangle("line", 10, 10, sw - 20, sh - 20)
end

function example.enabled()
    print("Example System Enabled!")
end

function example.disabled()
    print("Example System Disabled!")
end

return example