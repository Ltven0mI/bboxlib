local bboxlib = require("bboxlib")
local example = bboxlib.states.new({name="example"})

function example.draw()
    love.graphics.circle("line", 20, 20, 50)
end

function example.switchedTo()
  print("Switched To ExampleState")
end

function example.switchedFrom()
  print("Switched From ExampleState")
end

return example